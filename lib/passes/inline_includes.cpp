#include "passes/inline_includes.hh"

#include <algorithm>
#include <iostream>
#include <minizinc/astiterator.hh>
#include <minizinc/copy.hh>
#include <minizinc/file_utils.hh>
#include <minizinc/model.hh>
#include <minizinc/prettyprinter.hh>
#include <minizinc/solver.hh>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace MiniZinc;
using std::string;
using std::stringstream;
using std::unordered_map;
using std::unordered_set;
using std::vector;

namespace MznAnalyse {

InlineIncludes::InlineIncludes(bool lo) : local_only{lo} {}

Env* InlineIncludes::run(Env* e, std::ostream& log) {
  Model* model = e->model();
  string mzn_stdlib_dir = FileUtils::file_path(FileUtils::share_directory());

  unordered_set<string> seen;
  // Add data annotations
  for (size_t i = 0; i < model->size(); i++) {
    Item* item = model->operator[](i);
    if (IncludeI* ii = item->dynamicCast<IncludeI>()) {
      string filepath = ii->m()->filepath().c_str();
      if (seen.find(filepath) != seen.end()) continue;
      seen.insert(filepath);

      if (!local_only || filepath.rfind(mzn_stdlib_dir, 0) != 0) {
        ii->remove();
        Model* im = ii->m();
        for (size_t j = 0; j < im->size(); j++) {
          model->addItem(im->operator[](j));
        }
      }
    }
  }
  model->compact();
  return e;
}
};  // namespace MznAnalyse
