#include "passes/annotate_data_deps.hh"

#include <iostream>
#include <minizinc/copy.hh>
#include <minizinc/model.hh>
#include <minizinc/prettyprinter.hh>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace MiniZinc;
using std::string;
using std::stringstream;
using std::unordered_map;
using std::vector;

namespace MznAnalyse {

FunctionI* construct_data_ann(int nargs) {
  vector<VarDecl*> params;
  // int: depth
  params.push_back(new VarDecl(Location().introduce(),
                               new TypeInst(Location().introduce(), Type::parint()), "depth"));
  params.push_back(new VarDecl(Location().introduce(),
                               new TypeInst(Location().introduce(), Type::parint()), "index"));
  // string: type
  params.push_back(new VarDecl(Location().introduce(),
                               new TypeInst(Location().introduce(), Type::parstring()), "name"));
  // list of string: args
  for (int i = 0; i < nargs; i++) {
    std::stringstream ss;
    ss << "arg_" << i << "_";
    params.push_back(new VarDecl(
        Location().introduce(), new TypeInst(Location().introduce(), Type::parstring()), ss.str()));
  }
  TypeInst* ti = new TypeInst(Location().introduce(), Type::ann());

  return new FunctionI(Location().introduce(), ASTString("data"), ti, params);
}

FunctionI* construct_data_term_ann() {
  vector<VarDecl*> params;
  // int: depth
  params.push_back(new VarDecl(Location().introduce(),
                               new TypeInst(Location().introduce(), Type::parint()), "depth"));

  params.push_back(new VarDecl(Location().introduce(),
                               new TypeInst(Location().introduce(), Type::parint()), "index"));

  // string: type
  params.push_back(new VarDecl(Location().introduce(),
                               new TypeInst(Location().introduce(), Type::parstring()), "name"));

  // list of string: generators
  {
    vector<TypeInst*> t_idx = {new TypeInst(Location().introduce(), Type::parint())};
    TypeInst* ti = new TypeInst(Location().introduce(), Type::parstring());
    ti->setRanges(t_idx);
    params.push_back(new VarDecl(Location().introduce(), ti, "gens"));
  }

  // string: location
  params.push_back(new VarDecl(Location().introduce(),
                               new TypeInst(Location().introduce(), Type::parstring()), "locs"));

  // list of string: coefs
  {
    vector<TypeInst*> t_idx = {new TypeInst(Location().introduce(), Type::parint())};
    TypeInst* ti = new TypeInst(Location().introduce(), Type::parstring());
    ti->setRanges(t_idx);
    params.push_back(new VarDecl(Location().introduce(), ti, "coefs"));
  }

  // string: variable
  params.push_back(new VarDecl(
      Location().introduce(), new TypeInst(Location().introduce(), Type::parstring()), "variable"));

  TypeInst* ti = new TypeInst(Location().introduce(), Type::ann());

  return new FunctionI(Location().introduce(), ASTString("data"), ti, params);
}

Expression* without_anns(EnvI& envi, Expression* e) {
  Expression* e_copy = copy(envi, e);
  Expression::ann(e_copy).clear();
  return e_copy;
}

Expression* toStringLit(EnvI& envi, Expression* e) {
  std::stringstream ss;
  ss << *(without_anns(envi, e));
  return new StringLit(Location().introduce(), ss.str());
}

Expression* toShow(EnvI& envi, Expression* e) {
  return Call::a(Location().introduce(), "show", {without_anns(envi, e)});
}

Expression* data_ann(EnvI& envi, size_t depth, size_t index, string type,
                     vector<Expression*> exprs) {
  // Construct data(string: type, int: depth, list of string: args);
  vector<Expression*> args;
  args.push_back(IntLit::a(depth));
  args.push_back(IntLit::a(index));
  args.push_back(new StringLit(Location().introduce(), type));
  args.insert(args.end(), exprs.begin(), exprs.end());
  Call* ca = Call::a(Location().introduce(), "data", args);
  ca->type(Type::ann());
  return ca;
}

Expression* data_eq(EnvI& envi, size_t depth, size_t index, Expression* e) {
  if (Expression::isa<IntLit>(e) || Expression::isa<BoolLit>(e) || Expression::isa<SetLit>(e) ||
      Expression::isa<ArrayLit>(e) || Expression::isa<StringLit>(e)) {
    return data_ann(envi, depth, index, "lit", {toShow(envi, e)});
  }
  return data_ann(envi, depth, index, "eq", {toStringLit(envi, e), toShow(envi, e)});
}
Expression* data_assign(EnvI& envi, size_t depth, size_t index, Expression* e) {
  if (Expression::isa<IntLit>(e) || Expression::isa<BoolLit>(e) || Expression::isa<SetLit>(e) ||
      Expression::isa<ArrayLit>(e) || Expression::isa<StringLit>(e)) {
    return data_ann(envi, depth, index, "lit", {toShow(envi, e)});
  }
  return data_ann(envi, depth, index, "assign", {toStringLit(envi, e), toShow(envi, e)});
}

Expression* data_in(EnvI& envi, size_t depth, size_t index, Expression* id, Expression* in) {
  return data_ann(envi, depth, index, "in", {toStringLit(envi, id), toStringLit(envi, in)});
}

Expression* data_if(EnvI& envi, size_t depth, size_t index, Expression* where) {
  return data_ann(envi, depth, index, "if", {toStringLit(envi, where)});
}

// Expression* data_if_exists(EnvI& envi, size_t depth, Expression* e) {
//  Call* exists = e->cast<Call>();
//  Comprehension* comp = exists->args(0)->dyn_cast<Comprehension*>();
//
//  // exists(i in A, j in B where i < j) ( p[i,j] ) ->
//  //
//  // let {
//  //   int: tmp_i = arg_max(i in A) (exists(j in B where i < j) ( p[i,j] );
//  //   int: tmp_j = arg_max(j in B where tmp_i < j) ( p[tmp_i, j] )
//  // } in join(", ", ["i=" ++ show(A[tmp_i]), "j=" ++ show(B[tmp_j])]);
//
//  vector<Expression*> vars(comp->numberOfGenerators());
//  vector<Expression*> string_exps(2*comp->numberOfGenerators());
//
//  for (unsigned int i = comp->numberOfGenerators(); (i--) != 0U;) {
//    for (unsigned int j = comp->numberOfDecls(i); (j--) != 0U;) {
//      VarDecl* new_var = new VarDecl(Location().introduce(),
//          "tmp" + comp->);
//  }
//
//  Expression* in = Call::a(Location().introduce(), "concat", string_exps);
//  Let* let = new Let(envi, vars, in);
//  std::cerr << "Exists let: " << *exists << "\n======\n" << *let << "\n";
//
//  exit(EXIT_FAILURE);
//
//  return data_ann(envi, depth, "if_exists", { toStringLit(envi, where) });
//}

struct Frame {
  size_t depth;
  Expression* e;

  Frame(size_t d, Expression* expr) : depth{d}, e{expr} {}
};

/// Push all elements of \a v onto \a stack
template <class E>
void pushVec(size_t depth, std::vector<Frame>& stack, ASTExprVec<E> v) {
  for (unsigned int i = 0; i < v.size(); i++) {
    stack.emplace_back(depth + 1, v[i]);
  }
}

void annotateWithData(EnvI& envi, Expression* root) {
  std::vector<Frame> stack;
  stack.emplace_back(0, root);
  size_t index = 0;

  while (!stack.empty()) {
    Frame f = stack.back();
    size_t depth = f.depth;

    Expression* e = f.e;
    stack.pop_back();
    if (e == nullptr) {
      continue;
    }

    switch (Expression::eid(e)) {
      case Expression::E_INTLIT:
        break;
      case Expression::E_FLOATLIT:
        break;
      case Expression::E_SETLIT:
        pushVec(depth + 1, stack, Expression::template cast<SetLit>(e)->v());
        break;
      case Expression::E_BOOLLIT:
        break;
      case Expression::E_STRINGLIT:
        break;
      case Expression::E_ID:
        break;
      case Expression::E_ANON:
        break;
      case Expression::E_ARRAYLIT:
        for (unsigned int i = 0; i < Expression::cast<ArrayLit>(e)->size(); i++) {
          stack.emplace_back(depth + 1, (*Expression::cast<ArrayLit>(e))[i]);
        }
        break;
      case Expression::E_ARRAYACCESS:
        pushVec(depth + 1, stack, Expression::template cast<ArrayAccess>(e)->idx());
        stack.emplace_back(depth + 1, Expression::template cast<ArrayAccess>(e)->v());
        break;
      case Expression::E_COMP: {
        auto* comp = Expression::template cast<Comprehension>(e);
        stack.emplace_back(depth + 1, comp->e());

        for (unsigned int i = comp->numberOfGenerators(); (i--) != 0U;) {
          if (Expression::type(comp->e()).isvarbool() && comp->where(i) &&
              Expression::type(comp->where(i)).isPar()) {
            Expression::addAnnotation(comp->e(), data_if(envi, depth, index, comp->where(i)));
            index++;
          }
          if (Expression::type(comp->e()).isvarbool() && Expression::type(comp->in(i)).isPar()) {
            Expression::addAnnotation(comp->e(), data_eq(envi, depth, index, comp->in(i)));
            index++;
          }
          stack.emplace_back(depth + 1, comp->where(i));
          stack.emplace_back(depth + 1, comp->in(i));
          for (unsigned int j = comp->numberOfDecls(i); (j--) != 0U;) {
            if (Expression::type(comp->e()).isvarbool()) {
              if (Expression::type(comp->decl(i, j)).isPar()) {
                Expression::addAnnotation(comp->e(),
                                          data_assign(envi, depth, index, comp->decl(i, j)->id()));
                index++;
                if (Expression::type(comp->in(i)).isPar()) {
                  Expression::addAnnotation(
                      comp->e(), data_in(envi, depth, index, comp->decl(i, j)->id(), comp->in(i)));
                  index++;
                }
              }
            }
            stack.emplace_back(depth + 1, comp->decl(i, j));
          }
        }
      } break;
      case Expression::E_ITE: {
        ITE* ite = Expression::template cast<ITE>(e);
        stack.emplace_back(depth + 1, ite->elseExpr());
        for (size_t j = 0; j < ite->size(); j++) {
          if (Expression::type(ite->elseExpr()).isvarbool() &&
              Expression::type(ite->ifExpr(j)).isPar()) {
            Expression::addAnnotation(ite->elseExpr(), data_if(envi, depth, index,
                                                               new UnOp(Location().introduce(),
                                                                        UOT_NOT, ite->ifExpr(j))));
            index++;
          }
        }

        for (size_t i = 0; i < ite->size(); i++) {
          stack.emplace_back(depth + 1, ite->thenExpr(i));
          stack.emplace_back(depth + 1, ite->ifExpr(i));
          for (size_t j = 0; j < i; j++) {
            if (Expression::type(ite->thenExpr(i)).isvarbool() &&
                Expression::type(ite->ifExpr(j)).isPar()) {
              Expression::addAnnotation(
                  ite->thenExpr(i),
                  data_if(envi, depth, index,
                          new UnOp(Location().introduce(), UOT_NOT, ite->ifExpr(j))));
              index++;
            }
          }
          if (Expression::type(ite->thenExpr(i)).isvarbool() &&
              Expression::type(ite->ifExpr(i)).isPar()) {
            Expression* if_expr = ite->ifExpr(i);
            // Special behaviour for exists
            // if(if_expr->isa<Call>() && if_expr->cast<Call>()->id() == "exists")
            // {
            //  data_if_exists(envi, depth, if_expr);
            //}
            // Copy the condition directly
            Expression::addAnnotation(ite->thenExpr(i), data_if(envi, depth, index, if_expr));
            index++;
          }
        }
      } break;
      case Expression::E_BINOP:
        // Collect functional assignments
        {
          BinOp* bo = Expression::template cast<BinOp>(e);
          stack.emplace_back(depth + 1, bo->rhs());
          stack.emplace_back(depth + 1, bo->lhs());
        }
        break;
      case Expression::E_UNOP:
        stack.emplace_back(depth + 1, Expression::template cast<UnOp>(e)->e());
        break;
      case Expression::E_CALL:
        for (unsigned int i = 0; i < Expression::template cast<Call>(e)->argCount(); i++) {
          stack.emplace_back(depth + 1, Expression::template cast<Call>(e)->arg(i));
        }
        break;
      case Expression::E_VARDECL:
        stack.emplace_back(depth + 1, Expression::template cast<VarDecl>(e)->e());
        break;
      case Expression::E_LET:
        // Have to find solution for this
        stack.emplace_back(depth + 1, Expression::template cast<Let>(e)->in());
        pushVec(depth + 1, stack, Expression::template cast<Let>(e)->let());
        break;
      case Expression::E_TI:
        stack.emplace_back(depth + 1, Expression::template cast<TypeInst>(e)->domain());
        pushVec(depth + 1, stack, Expression::template cast<TypeInst>(e)->ranges());
        break;
      case Expression::E_TIID:
        break;
    }
  }
}

AnnotateDataDeps::AnnotateDataDeps() {}

MiniZinc::Env* AnnotateDataDeps::run(MiniZinc::Env* e, std::ostream& log) {
  // Add data annotations
  Model* m = e->model();
  for (ConstraintI& ci : m->constraints()) {
    annotateWithData(e->envi(), ci.e());
  }
  m->addItem(construct_data_ann(1));
  m->addItem(construct_data_ann(2));
  m->compact();

  return e;
}
};  // namespace MznAnalyse
